#!/usr/bin/env python

from distutils.core import setup

setup(name='django-mediagenerator-extras',
      version='0.1',
      description='additional compressors for django-mediagenerator',
      author='Evgeniy Kirov',
      author_email='evg.kirov@gmail.com',
      url='https://bitbucket.org/eXtractor/django-mediagenerator-extras',
      license='public domain',
      packages=['mediagenerator_extras'],
      requires=['cssutils', 'cssprefixer', 'cssmin'])
