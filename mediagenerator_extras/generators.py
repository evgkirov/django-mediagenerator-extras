from django.conf import settings
from mediagenerator.generators.bundles.base import Filter


CSSPREFIXER_MINIFY = getattr(settings, 'CSSPREFIXER_MINIFY', True)
CLOSURE_COMPILATION_LEVEL = getattr(settings, 'CLOSURE_COMPILATION_LEVEL',
                                    'SIMPLE_OPTIMIZATIONS')


class ClosureWebFilter(Filter):
    def __init__(self, **kwargs):
        self.config(kwargs, compilation_level=CLOSURE_COMPILATION_LEVEL)
        super(ClosureWebFilter, self).__init__(**kwargs)
        assert self.filetype == 'js', (
            'ClosureWeb only supports compilation to js. '
            'The parent filter expects "%s".' % self.filetype)

    def get_output(self, variation):
        import urllib
        import urllib2
        url = 'http://closure-compiler.appspot.com/compile'
        for input in self.get_input(variation):
            values = {
                'js_code': input.encode('utf-8'),
                'compilation_level': CLOSURE_COMPILATION_LEVEL,
                'output_format': 'text',
                'output_info': 'compiled_code',
            }
            data = urllib.urlencode(values)
            req = urllib2.Request(url, data)
            response = urllib2.urlopen(req)
            yield response.read()


class CssminFilter(Filter):

    def get_output(self, variation):
        from cssmin import cssmin
        for input in self.get_input(variation):
            yield cssmin(input)




class CssprefixerFilter(Filter):

    def get_output(self, variation):
        import cssprefixer
        for input in self.get_input(variation):
            yield cssprefixer.process(input, debug=False, minify=CSSPREFIXER_MINIFY)
